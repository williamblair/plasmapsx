/*
 * main.c
 */

#include <stdlib.h>
#include <libmath.h>
#include <libgte.h>
#include <libgpu.h>
#include <libgs.h>
#include <libetc.h>

#define S_WIDTH  320
#define S_HEIGHT 240

#define OT_LENGTH 10
#define PACKETMAX 1000

#define PACKET_SIZE 64

// TODO - Look at PSYQ sprite example and load texture
// alternatively - test how many pixels we can draw!!!!


GsOT     ot[2]; // 2 order tables
GsOT_TAG ot_tag[2][1<<OT_LENGTH];
PACKET   packet_area[2][PACKETMAX];

u_long __ramsize   = 0x00200000; // force 2MB RAM
u_long __stacksize = 0x00004000; // force 16KB stack

short curbuf;

u_short tpage_id; // texture page id
u_short clut_id;  // texture CLUT id

POLY_FT4 spr; // texture mapped polygon
SVECTOR pos; // position of the sprite

#define RGB16(r,g,b) \
	((r) << 10 | (g) << 5 | (b))

static inline u_long rgb24_to_rgb16(int val)
//static inline u_long rgb24_to_rgb16(u_char r, u_char g, u_char b)
{
	//u_long val;
	int R, G, B;
    
	/* Get the R component and scale */
    R = (val >> 16) & 0xFF;
    R *= (31.0/255.0);

    /* Get the G component and scale */
    G = (val >> 8) & 0xFF;
    G *= (31.0/255.0);

    /* Get the B Component and scale */
    B = val & 0xFF;
    B *= (31.0/255.0);

    /* Stuff into 16 bits */
	val = (u_long)((B << 11) | 
           (G << 6)  |
           R);
	
	//printf("RGB: %d,%d,%d,    0x%X\n",R,G,B,val);
	printf("0x%X\n", val);
	
    return val;
}

#if 0
// integers in 24bit rgb format of a HSV spectrum
// XTerm Colors
u_int hsv_lookup_table[256] = {
    0x000000,
    0x800000,
    0x008000,
    0x808000,
    0x000080,
    0x800080,
    0x008080,
    0xc0c0c0,
    0x808080,
    0xff0000,
    0x00ff00,
    0xffff00,
    0x0000ff,
    0xff00ff,
    0x00ffff,
    0xffffff,
    0x000000,
    0x00005f,
    0x000087,
    0x0000af,
    0x0000d7,
    0x0000ff,
    0x005f00,
    0x005f5f,
    0x005f87,
    0x005faf,
    0x005fd7,
    0x005fff,
    0x008700,
    0x00875f,
    0x008787,
    0x0087af,
    0x0087d7,
    0x0087ff,
    0x00af00,
    0x00af5f,
    0x00af87,
    0x00afaf,
    0x00afd7,
    0x00afff,
    0x00d700,
    0x00d75f,
    0x00d787,
    0x00d7af,
    0x00d7d7,
    0x00d7ff,
    0x00ff00,
    0x00ff5f,
    0x00ff87,
    0x00ffaf,
    0x00ffd7,
    0x00ffff,
    0x5f0000,
    0x5f005f,
    0x5f0087,
    0x5f00af,
    0x5f00d7,
    0x5f00ff,
    0x5f5f00,
    0x5f5f5f,
    0x5f5f87,
    0x5f5faf,
    0x5f5fd7,
    0x5f5fff,
    0x5f8700,
    0x5f875f,
    0x5f8787,
    0x5f87af,
    0x5f87d7,
    0x5f87ff,
    0x5faf00,
    0x5faf5f,
    0x5faf87,
    0x5fafaf,
    0x5fafd7,
    0x5fafff,
    0x5fd700,
    0x5fd75f,
    0x5fd787,
    0x5fd7af,
    0x5fd7d7,
    0x5fd7ff,
    0x5fff00,
    0x5fff5f,
    0x5fff87,
    0x5fffaf,
    0x5fffd7,
    0x5fffff,
    0x870000,
    0x87005f,
    0x870087,
    0x8700af,
    0x8700d7,
    0x8700ff,
    0x875f00,
    0x875f5f,
    0x875f87,
    0x875faf,
    0x875fd7,
    0x875fff,
    0x878700,
    0x87875f,
    0x878787,
    0x8787af,
    0x8787d7,
    0x8787ff,
    0x87af00,
    0x87af5f,
    0x87af87,
    0x87afaf,
    0x87afd7,
    0x87afff,
    0x87d700,
    0x87d75f,
    0x87d787,
    0x87d7af,
    0x87d7d7,
    0x87d7ff,
    0x87ff00,
    0x87ff5f,
    0x87ff87,
    0x87ffaf,
    0x87ffd7,
    0x87ffff,
    0xaf0000,
    0xaf005f,
    0xaf0087,
    0xaf00af,
    0xaf00d7,
    0xaf00ff,
    0xaf5f00,
    0xaf5f5f,
    0xaf5f87,
    0xaf5faf,
    0xaf5fd7,
    0xaf5fff,
    0xaf8700,
    0xaf875f,
    0xaf8787,
    0xaf87af,
    0xaf87d7,
    0xaf87ff,
    0xafaf00,
    0xafaf5f,
    0xafaf87,
    0xafafaf,
    0xafafd7,
    0xafafff,
    0xafd700,
    0xafd75f,
    0xafd787,
    0xafd7af,
    0xafd7d7,
    0xafd7ff,
    0xafff00,
    0xafff5f,
    0xafff87,
    0xafffaf,
    0xafffd7,
    0xafffff,
    0xd70000,
    0xd7005f,
    0xd70087,
    0xd700af,
    0xd700d7,
    0xd700ff,
    0xd75f00,
    0xd75f5f,
    0xd75f87,
    0xd75faf,
    0xd75fd7,
    0xd75fff,
    0xd78700,
    0xd7875f,
    0xd78787,
    0xd787af,
    0xd787d7,
    0xd787ff,
    0xd7af00,
    0xd7af5f,
    0xd7af87,
    0xd7afaf,
    0xd7afd7,
    0xd7afff,
    0xd7d700,
    0xd7d75f,
    0xd7d787,
    0xd7d7af,
    0xd7d7d7,
    0xd7d7ff,
    0xd7ff00,
    0xd7ff5f,
    0xd7ff87,
    0xd7ffaf,
    0xd7ffd7,
    0xd7ffff,
    0xff0000,
    0xff005f,
    0xff0087,
    0xff00af,
    0xff00d7,
    0xff00ff,
    0xff5f00,
    0xff5f5f,
    0xff5f87,
    0xff5faf,
    0xff5fd7,
    0xff5fff,
    0xff8700,
    0xff875f,
    0xff8787,
    0xff87af,
    0xff87d7,
    0xff87ff,
    0xffaf00,
    0xffaf5f,
    0xffaf87,
    0xffafaf,
    0xffafd7,
    0xffafff,
    0xffd700,
    0xffd75f,
    0xffd787,
    0xffd7af,
    0xffd7d7,
    0xffd7ff,
    0xffff00,
    0xffff5f,
    0xffff87,
    0xffffaf,
    0xffffd7,
    0xffffff,
    0x080808,
    0x121212,
    0x1c1c1c,
    0x262626,
    0x303030,
    0x3a3a3a,
    0x444444,
    0x4e4e4e,
    0x585858,
    0x606060,
    0x666666,
    0x767676,
    0x808080,
    0x8a8a8a,
    0x949494,
    0x9e9e9e,
    0xa8a8a8,
    0xb2b2b2,
    0xbcbcbc,
    0xc6c6c6,
    0xd0d0d0,
    0xdadada,
    0xe4e4e4,
    0xeeeeee
};
#endif

// hsv2rgb colors
//u_int hsv_lookup_table[256] = {
u_short hsv_lookup_table[256] = {
    0x1F,
    0x1F,
    0x5F,
    0x5F,
    0x9F,
    0xDF,
    0xDF,
    0x11F,
    0x11F,
    0x15F,
    0x19F,
    0x19F,
    0x1DF,
    0x21F,
    0x21F,
    0x25F,
    0x25F,
    0x29F,
    0x2DF,
    0x2DF,
    0x31F,
    0x35F,
    0x35F,
    0x39F,
    0x39F,
    0x3DF,
    0x41F,
    0x41F,
    0x45F,
    0x45F,
    0x49F,
    0x4DF,
    0x4DF,
    0x51F,
    0x55F,
    0x55F,
    0x59F,
    0x59F,
    0x5DF,
    0x61F,
    0x61F,
    0x65F,
    0x69F,
    0x69F,
    0x6DF,
    0x6DF,
    0x71F,
    0x75F,
    0x75F,
    0x79F,
    0x7DF,
    0x7DE,
    0x7DD,
    0x7DD,
    0x7DC,
    0x7DB,
    0x7DB,
    0x7DA,
    0x7DA,
    0x7D9,
    0x7D8,
    0x7D8,
    0x7D7,
    0x7D6,
    0x7D6,
    0x7D5,
    0x7D5,
    0x7D4,
    0x7D3,
    0x7D3,
    0x7D2,
    0x7D1,
    0x7D1,
    0x7D0,
    0x7D0,
    0x7CF,
    0x7CE,
    0x7CE,
    0x7CD,
    0x7CD,
    0x7CC,
    0x7CB,
    0x7CB,
    0x7CA,
    0x7C9,
    0x7C9,
    0x7C8,
    0x7C8,
    0x7C7,
    0x7C6,
    0x7C6,
    0x7C5,
    0x7C4,
    0x7C4,
    0x7C3,
    0x7C3,
    0x7C2,
    0x7C1,
    0x7C1,
    0x7C0,
    0x7C0,
    0x7C0,
    0xFC0,
    0xFC0,
    0x17C0,
    0x1FC0,
    0x1FC0,
    0x27C0,
    0x27C0,
    0x2FC0,
    0x37C0,
    0x37C0,
    0x3FC0,
    0x47C0,
    0x47C0,
    0x4FC0,
    0x4FC0,
    0x57C0,
    0x5FC0,
    0x5FC0,
    0x67C0,
    0x6FC0,
    0x6FC0,
    0x77C0,
    0x77C0,
    0x7FC0,
    0x87C0,
    0x87C0,
    0x8FC0,
    0x8FC0,
    0x97C0,
    0x9FC0,
    0x9FC0,
    0xA7C0,
    0xAFC0,
    0xAFC0,
    0xB7C0,
    0xB7C0,
    0xBFC0,
    0xC7C0,
    0xC7C0,
    0xCFC0,
    0xD7C0,
    0xD7C0,
    0xDFC0,
    0xDFC0,
    0xE7C0,
    0xEFC0,
    0xEFC0,
    0xF7C0,
    0xFFC0,
    0xFF80,
    0xFF40,
    0xFF40,
    0xFF00,
    0xFEC0,
    0xFEC0,
    0xFE80,
    0xFE80,
    0xFE40,
    0xFE00,
    0xFE00,
    0xFDC0,
    0xFD80,
    0xFD80,
    0xFD40,
    0xFD40,
    0xFD00,
    0xFCC0,
    0xFCC0,
    0xFC80,
    0xFC40,
    0xFC40,
    0xFC00,
    0xFC00,
    0xFBC0,
    0xFB80,
    0xFB80,
    0xFB40,
    0xFB40,
    0xFB00,
    0xFAC0,
    0xFAC0,
    0xFA80,
    0xFA40,
    0xFA40,
    0xFA00,
    0xFA00,
    0xF9C0,
    0xF980,
    0xF980,
    0xF940,
    0xF900,
    0xF900,
    0xF8C0,
    0xF8C0,
    0xF880,
    0xF840,
    0xF840,
    0xF800,
    0xF800,
    0xF800,
    0xF840,
    0xF840,
    0xF880,
    0xF8C0,
    0xF8C0,
    0xF900,
    0xF900,
    0xF940,
    0xF980,
    0xF980,
    0xF9C0,
    0xFA00,
    0xFA00,
    0xFA40,
    0xFA40,
    0xFA80,
    0xFAC0,
    0xFAC0,
    0xFB00,
    0xFB40,
    0xFB40,
    0xFB80,
    0xFB80,
    0xFBC0,
    0xFC00,
    0xFC00,
    0xFC40,
    0xFC40,
    0xFC80,
    0xFCC0,
    0xFCC0,
    0xFD00,
    0xFD40,
    0xFD40,
    0xFD80,
    0xFD80,
    0xFDC0,
    0xFE00,
    0xFE00,
    0xFE40,
    0xFE80,
    0xFE80,
    0xFEC0,
    0xFEC0,
    0xFF00,
    0xFF40,
    0xFF40,
    0xFF80,
    0xFFC0,
    0xFF80,
    0xFF40,
    0xFF40,
    0xFF00,
    0xFEC0
};

// the clut
u_short clut[256];
void init_clut(void)
{
	/* Fill with RGB */
	//u_char r = 0, g = 0, b = 0;
	int i;
	u_short val;
	//for (i=0; i<256; ++i) {
	//	clut[i] = hsv_lookup_table[i];
	//}
	memcpy(clut, hsv_lookup_table, sizeof(hsv_lookup_table));
	
	/* Upload to VRAM */
	clut_id = LoadClut((u_long*)clut, 704, 500);
}

// the texture
#define T_WIDTH 240
#define T_HEIGHT 240
//u_char texture[T_WIDTH*T_HEIGHT] = {
//};
#include "plasma_array.c"
void init_texture(void)
{
	int i = 0;
	int x,y;
	u_long index;
	
	// calculate plasma test
#if 0
	for (y=0; y<T_HEIGHT; ++y)
	{
		for (x=0; x<T_WIDTH; ++x)
		{
			index = (u_long)((128.0 + (128.0 * sin(x/8.0)) +
									128.0 + (128.0 * sin(y/8.0))
									) / 2)%256;
			
			//printf("X,Y: %d,%d    Index: %d\n", x,y, index);
			//printf("%d\n", index);
			
			texture[y*T_WIDTH+x] = index;
		}
	}
#endif
	
	// REMOVE ME
	//for (i=0; i<T_WIDTH*T_HEIGHT; ++i) {
	//	printf("%d\n", texture[i]);
	//}
	
	//printf("Outside for fors\n");
	
	// upload the texture to vram
	tpage_id = LoadTPage(
		(u_long*)texture,           // pointer to texture pattern start address
		1,                 // Bit Depth - 0 = 4bit, 1 = 8bit, 2 = 16bit
		0,                 // Semitransparency rate
		704, 0,            // x,y in framebuffer
		T_WIDTH, T_HEIGHT  // texture pattern size - width represents number of pixels
	);
	
}

/* Initialize the PSX */
void init(void)
{
    // init the controller
    PadInit(0);

    // Set the video mode
    if (*(char*)0xBFC7FF52 == 'E') SetVideoMode(MODE_PAL);
    else                           SetVideoMode(MODE_NTSC);

    // init graphics, noninterlaced and for resolution 640x480
    GsInitGraph(S_WIDTH, S_HEIGHT, GsNONINTER|GsOFSGPU, 1, 0);
    GsDefDispBuff(0,0,0,0);

    // set order table stuff
    ot[0].length = ot[1].length = OT_LENGTH;
    ot[0].org = ot_tag[0];
    ot[1].org = ot_tag[1];

    GsClearOt(0,0,&ot[0]);
    GsClearOt(0,0,&ot[1]);

    // Load the font
    FntLoad(960,256);
    SetDumpFnt(FntOpen(5, 20, 320, 240, 0, 512));
}

int main(void)
{
	int frame_counter = 0;
	int i;
    
	init();

	// initialize the poly
	SetPolyFT4(&spr);
	SetShadeTex(&spr, 1); // shade tex disable
	
	// set the position
	pos.vx = 40; pos.vy = 0;
	
	// load the clut into the framebuffer
	init_clut();
	
	// load the texture into the framebuffer
	init_texture();
	
	// set its texture and CLUT ids
	spr.tpage = tpage_id;
	spr.clut = clut_id;
	
	// set its uv (offset within the texture, for sprite frames)
	// and its size
	setUVWH(&spr, 0, 0, T_WIDTH, T_HEIGHT);
	
	// set its position
	setXYWH(&spr, pos.vx, pos.vy, T_WIDTH, T_HEIGHT);
	
    for(;;)
    {
        
		curbuf = GsGetActiveBuff();
		GsSetWorkBase((PACKET*)packet_area[curbuf]);
		GsClearOt(0,0,&ot[curbuf]);
		GsSwapDispBuff();
		GsSortClear(0,0,0, &ot[curbuf]);

		GsDrawOt(&ot[curbuf]);
		
		// change the clut
		frame_counter++;
		for (i=0; i<256; ++i) {
			clut[i] = hsv_lookup_table[(i+frame_counter)&0xFF];
		}
		// do we need to get a new id for this?
		LoadClut((u_long*)clut, 704, 500);
		
		// draw primitive
		DrawPrim(&spr);
		
		DrawSync(0);
		VSync(0);
    }

    return 0;
}

